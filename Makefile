# Install dependencies
setup:
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger

	cp -r $(GOPATH)/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.14.3/third_party/googleapis/google/ third_party/google

	cp -r  $(GOPATH)/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.14.3/protoc-gen-swagger/options/ third_party/protoc-gen-swagger/options/


# Generate protobuf
proto:
	protoc --proto_path=api/proto/v1 --proto_path=third_party --govalidators_out=pkg/api/v1 --go_out=plugins=grpc:pkg/api/v1 mqtt.proto

	protoc --proto_path=api/proto/v1 --proto_path=third_party --grpc-gateway_out=logtostderr=true:pkg/api/v1 mqtt.proto

	protoc --proto_path=api/proto/v1 --proto_path=third_party --swagger_out=logtostderr=true:api/swagger/v1 mqtt.proto

# Build the binary locally for testing
build-server:
	go build -o build/mqtt cmd/mqtt/server/*.go 

# Create docker image and upload to docker hub
docker:
	docker build -t mqtt:v1 .
	docker tag mqtt:v1 chip2cloud/internal:mqtt-v1
	docker push chip2cloud/internal:mqtt-v1

# Deploy to kubernetes
deploy:
	kubectl create -f deploy/k8/configmap.yaml
	kubectl create -f deploy/k8/deployment.yaml
	kubectl create -f deploy/k8/service.yaml
