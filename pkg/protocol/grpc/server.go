package grpc

import (
	"context"
	"log"
	"net"
	"os"
	"os/signal"

	"google.golang.org/grpc"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	v1 "gitlab.com/chip2cloud/aws-iot-mqtt/pkg/api/v1"
)

// RunServer runs gRPC service to publish the service
func RunServer(ctx context.Context, v1API v1.PublishServiceServer, port string, client MQTT.Client) error {
	listen, err := net.Listen("tcp", ":"+port)
	if err != nil {
		return err
	}

	// register service with interceptor
	//server := grpc.NewServer(grpc.UnaryInterceptor(interceptor.AuthInterceptor))
	server := grpc.NewServer()
	v1.RegisterPublishServiceServer(server, v1API)

	// graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			// sig is a ^C, handle it
			log.Println("shutting down gRPC server...")

			server.GracefulStop()

			log.Println("Disconnecting MQTT ...")
			client.Disconnect(250)

			<-ctx.Done()
		}
	}()

	// start gRPC server
	log.Println("starting gRPC server...")
	return server.Serve(listen)
}
