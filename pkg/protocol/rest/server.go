package rest

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"

	"google.golang.org/grpc"

	v1 "gitlab.com/chip2cloud/aws-iot-mqtt/pkg/api/v1"
)

// RunServer runs HTTP/REST gateway
func RunServer(ctx context.Context, grpcPort, httpPort string) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Create a HTTP request router/multiplexer to handle route based on URL path
	// Extract email from the header and add to context
	mux := runtime.NewServeMux()

	opts := []grpc.DialOption{grpc.WithInsecure()}

	// On receiving HTTP request, make a HTTP/2 call to gRPC server running on the localhost
	// HTTP gateway and gRPC service will be part of same binary
	// RegisterPublishServiceHandlerFromEndpoint is the HTTP/REST handler created automatically grpc-gateway
	if err := v1.RegisterPublishServiceHandlerFromEndpoint(ctx, mux, "localhost:"+grpcPort, opts); err != nil {
		log.Fatalf("failed to register gRPC handler: %v", err)
	}

	// Serve the HTTP endpoint
	srv := &http.Server{
		Addr:    ":" + httpPort,
		Handler: mux,
	}

	// graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			// sig is a ^C, handle it
		}

		_, cancel := context.WithTimeout(ctx, 5*time.Second)
		defer cancel()

		_ = srv.Shutdown(ctx)
	}()

	log.Println("starting HTTP/REST gateway...")
	return srv.ListenAndServe()
}
