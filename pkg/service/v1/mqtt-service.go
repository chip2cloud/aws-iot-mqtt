package v1

import (
	"context"
	"encoding/json"
	"log"

	"google.golang.org/grpc/codes"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	v1 "gitlab.com/chip2cloud/aws-iot-mqtt/pkg/api/v1"
)

type mqttServiceServer struct {
	client MQTT.Client
}

// NewMqttServiceServer creates mqtt service
func NewMqttServiceServer(c MQTT.Client) v1.PublishServiceServer {
	return &mqttServiceServer{
		client: c,
	}
}

func (m *mqttServiceServer) Publish(ctx context.Context, req *v1.PublishRequest) (*v1.PublishResponse, error) {

	var responseMessage string
	var responseCode uint32

	topic := req.GetTopic()
	data := req.GetData()

	bytes, _ := json.Marshal(data)

	if token := m.client.Publish(topic, 0, false, string(bytes)); token.Wait() && token.Error() != nil {
		responseCode = uint32(codes.Unavailable)
		responseMessage = "Couldn't publish the message to AWS IoT"
		log.Println(token.Error())
	} else {
		responseCode = uint32(codes.OK)
		responseMessage = "Request sent to AWS IoT"
	}

	return &v1.PublishResponse{
		Status:  responseCode,
		Message: responseMessage,
	}, nil

}
