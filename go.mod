module gitlab.com/chip2cloud/aws-iot-mqtt

go 1.14

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/golang/protobuf v1.4.2
	github.com/grpc-ecosystem/grpc-gateway v1.14.6
	github.com/mwitkow/go-proto-validators v0.3.0
	github.com/piyush-saurabh/golang-grpc-http-api v0.0.0-20200322054630-23b60a8c4736
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	google.golang.org/genproto v0.0.0-20200808173500-a06252235341
	google.golang.org/grpc v1.31.0
)
