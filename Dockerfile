FROM golang:alpine AS builder
RUN apk --no-cache add ca-certificates
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN CGO_ENABLED=0 GOOS=linux go build -o build/mqtt cmd/mqtt/server/*.go

FROM scratch AS production
WORKDIR /app
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /app/build .