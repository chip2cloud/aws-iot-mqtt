# aws-iot-mqtt

This microservice communicates with AWT IoT and publishes a message on specific topic

## Execution
```bash
./build/mqtt --grpc-port 8883 --http-port 8884 --aws-iot-host endpoint.iot.us-east-1.amazonaws.com --aws-iot-port 8883 --aws-public-cert /path/to/certificate.pem.crt --aws-private-key /path/to/private.pem.key
```