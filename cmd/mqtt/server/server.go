package main

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"log"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"

	"gitlab.com/chip2cloud/aws-iot-mqtt/pkg/protocol/grpc"
	"gitlab.com/chip2cloud/aws-iot-mqtt/pkg/protocol/rest"
	v1 "gitlab.com/chip2cloud/aws-iot-mqtt/pkg/service/v1"
)

// Config is configuration for Server
type Config struct {
	// gRPC server start parameters section
	// gRPC is TCP port to listen by gRPC server
	GRPCPort string

	// HTTP/REST gateway start parameters section
	// HTTPPort is TCP port to listen by HTTP/REST gateway
	HTTPPort string

	// AWS IOT host
	AWSIotHost string

	// AWS IoT port
	AWSIotPort int

	// AWS IoT certificate
	AWSPublicCert string

	// AWS IoT private key
	AWSPrivateKey string

	// MQTT Client ID
	MqttClientID string
}

// RunServer runs gRPC server and HTTP gateway
func RunServer() error {
	ctx := context.Background()

	// get configuration
	var cfg Config
	flag.StringVar(&cfg.GRPCPort, "grpc-port", "", "gRPC port to bind")
	flag.StringVar(&cfg.HTTPPort, "http-port", "", "HTTP port to bind")
	flag.StringVar(&cfg.AWSIotHost, "aws-iot-host", "", "AWS IoT Host IP/Domain")
	flag.IntVar(&cfg.AWSIotPort, "aws-iot-port", 8883, "AWS IoT MQTT port")
	flag.StringVar(&cfg.AWSPublicCert, "aws-public-cert", "", "AWS IoT certificate")
	flag.StringVar(&cfg.AWSPrivateKey, "aws-private-key", "", "AWS IoT private key")
	flag.StringVar(&cfg.MqttClientID, "mqtt-client-id", "", "MQTT Unique Client ID")

	flag.Parse()

	if len(cfg.GRPCPort) == 0 {
		return fmt.Errorf("Invalid TCP port for gRPC server: '%s'", cfg.GRPCPort)
	}

	if len(cfg.HTTPPort) == 0 {
		return fmt.Errorf("Invalid TCP port for HTTP gateway: '%s'", cfg.HTTPPort)
	}

	cer, err := tls.LoadX509KeyPair(cfg.AWSPublicCert, cfg.AWSPrivateKey)

	if err != nil {
		panic(err)
	}

	// AWS IOT settings
	brokerURL := fmt.Sprintf("tcps://%s:%d", cfg.AWSIotHost, cfg.AWSIotPort)

	connOpts := MQTT.NewClientOptions().AddBroker(brokerURL)
	connOpts.SetClientID(cfg.MqttClientID)
	connOpts.SetMaxReconnectInterval(1 * time.Second)
	connOpts.SetTLSConfig(&tls.Config{Certificates: []tls.Certificate{cer}})

	mqttClient := MQTT.NewClient(connOpts)
	if token := mqttClient.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	log.Println("MQTT Connected")

	v1API := v1.NewMqttServiceServer(mqttClient)

	// Start the HTTP gateway
	// This gateway will listen on HTTP port
	// gRPC service port is passed to make gRPC call from gateway to the service
	go func() {
		_ = rest.RunServer(ctx, cfg.GRPCPort, cfg.HTTPPort)
	}()

	return grpc.RunServer(ctx, v1API, cfg.GRPCPort, mqttClient)
}
